import UIKit

var barrierString = "barrier"
var difficulty: CGFloat = 5
let easy: CGFloat = 5
let normal: CGFloat = 4
let hard: CGFloat = 3
let insane: CGFloat = 1.5
let marlboroFont = UIFont(name: "Marlboro", size: 20)

enum Keys: String {
    case userNicknames = "nickname"
    case highScore = "scores"
    case difficulty = "difficulty"
    case barrier = "barrier"
    case classHighScores = "highscore"
}

import UIKit

extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.y, y: -origin.x,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return rotatedImage ?? self
        }

        return self
    }
}

extension UIViewController {
    func dropShadow(color: UIColor = .black, opacity: Float = 0.5, offSet: CGSize = CGSize(width: 5, height: 5), radius: CGFloat = 5, scale: Bool = true) {
        self.view.layer.masksToBounds = false
        self.view.layer.shadowColor = color.cgColor
        self.view.layer.shadowOpacity = opacity
        self.view.layer.shadowOffset = offSet
        self.view.layer.shadowRadius = radius
        self.view.layer.shadowPath = UIBezierPath(roundedRect: self.view.bounds, cornerRadius: radius).cgPath
        self.view.layer.shouldRasterize = true
        self.view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func randomColor() -> UIColor {
        return UIColor(
                        displayP3Red: CGFloat.random(in: 0 ... 1),
                        green: CGFloat.random(in: 0 ... 1),
                        blue: CGFloat.random(in: 0 ... 1),
                        alpha: 1)
    }
}

extension UIView {
    func roundCorner(radius: CGFloat = 20) {
        self.layer.cornerRadius = radius
    }
    
    func dropShadow(color: UIColor = .black, opacity: Float = 0.7, offSet: CGSize = CGSize(width: 5, height: 5), radius: CGFloat = 5, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func addGradient(radius: CGFloat = 20) {
        let gradient = CAGradientLayer()
        
        gradient.colors = [UIColor.systemPink.cgColor, UIColor.brown.cgColor]
        
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = self.bounds
        gradient.cornerRadius = radius
        self.layer.addSublayer(gradient)
    }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

extension String {
    
    func locatized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}

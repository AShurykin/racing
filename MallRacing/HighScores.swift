import UIKit

class Highscores: Codable {
    let name: String
    let score: Int
    let time: String
    
    init(name: String, score: Int, time: String) {
        self.name = name
        self.score = score
        self.time = time
    }
    
    enum CodingKeys: String, CodingKey {
        case name, score, time
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.name, forKey: .name)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.time, forKey: .time)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try (container.decodeIfPresent(String.self, forKey: .name) ?? "name")
        self.score = try (container.decodeIfPresent(Int.self, forKey: .score) ?? 0)
        self.time = try (container.decodeIfPresent(String.self, forKey: .time) ?? "time")
    }
}


import UIKit

class ThirdViewControllerManager {
    
    static let shared = ThirdViewControllerManager()
    private init() {}
    
    
    func changeBarrier(_ barrierButton: UIButton,
                       _ ballButton: UIButton,
                       _ tireButton: UIButton) {
        if let currentBarrier = UserDefaults.standard.object(forKey: Keys.barrier.rawValue) as? String {
            switch currentBarrier {
            case "barrier":
                barrierButton.backgroundColor = .red
                barrierButton.backgroundColor?.withAlphaComponent(0.5)
                ballButton.backgroundColor = nil
                tireButton.backgroundColor = nil
            case "football":
                ballButton.backgroundColor = .red
                ballButton.backgroundColor?.withAlphaComponent(0.5)
                barrierButton.backgroundColor = nil
                tireButton.backgroundColor = nil
            case "tire":
                tireButton.backgroundColor = .red
                tireButton.backgroundColor?.withAlphaComponent(0.5)
                barrierButton.backgroundColor = nil
                ballButton.backgroundColor = nil
            default:
                print("Error")
            }
        } else {
            UserDefaults.standard.set(barrierString, forKey: Keys.barrier.rawValue)
            barrierButton.backgroundColor = .red
            barrierButton.backgroundColor?.withAlphaComponent(0.5)
            ballButton.backgroundColor = nil
        }
    }
    
    func changeDifficulty(_ easyButton: UIButton,
                          _ normalButton: UIButton,
                          _ hardButton: UIButton,
                          _ insaneButton: UIButton) {
        if let currentDifficulty = UserDefaults.standard.object(forKey: Keys.difficulty.rawValue) as? CGFloat {
            switch currentDifficulty {
            case easy:
                easyButton.backgroundColor = .red
                easyButton.backgroundColor?.withAlphaComponent(0.5)
                normalButton.backgroundColor = nil
                hardButton.backgroundColor = nil
                insaneButton.backgroundColor = nil
            case normal:
                normalButton.backgroundColor = .red
                normalButton.backgroundColor?.withAlphaComponent(0.5)
                easyButton.backgroundColor = nil
                hardButton.backgroundColor = nil
                insaneButton.backgroundColor = nil
            case hard:
                hardButton.backgroundColor = .red
                hardButton.backgroundColor?.withAlphaComponent(0.5)
                easyButton.backgroundColor = nil
                normalButton.backgroundColor = nil
                insaneButton.backgroundColor = nil
            case insane:
                insaneButton.backgroundColor = .red
                insaneButton.backgroundColor?.withAlphaComponent(0.5)
                easyButton.backgroundColor = nil
                normalButton.backgroundColor = nil
                hardButton.backgroundColor = nil
            default:
                print("Error")
            }
        } else {
            UserDefaults.standard.set(difficulty, forKey: Keys.difficulty.rawValue)
            easyButton.backgroundColor = .red
            easyButton.backgroundColor?.withAlphaComponent(0.5)
            normalButton.backgroundColor = nil
            hardButton.backgroundColor = nil
            insaneButton.backgroundColor = nil
        }
    }
}

import UIKit
import CoreMotion

class MotionManager {
    static let shared = MotionManager()
    private init() {}
    
    func addHorizontalCarMotions(motionManager: CMMotionManager,
                                 carView: UIImageView,
                                 raceStatus: RaceStatus) {
        motionManager.startAccelerometerUpdates()
        let timer = Timer.scheduledTimer(withTimeInterval: 0.005, repeats: true) { (timer) in
            guard let horizontalMotion = motionManager.accelerometerData?.acceleration.x else {
                return
            }
            if horizontalMotion < -0.1 && horizontalMotion > -0.2 {
                carView.center.x -= 0.1
            } else if horizontalMotion > 0.1 && horizontalMotion < 0.2 {
                carView.center.x += 0.1
            } else if horizontalMotion < -0.2 && horizontalMotion > -0.25 {
                carView.center.x -= 0.3
            } else if horizontalMotion > 0.2 && horizontalMotion < 0.25 {
                carView.center.x += 0.3
            } else if horizontalMotion < -0.25 && horizontalMotion > -0.3 {
                carView.center.x -= 0.6
            } else if horizontalMotion > 0.25 && horizontalMotion < 0.3 {
                carView.center.x += 0.6
            } else if horizontalMotion < -0.3 && horizontalMotion > -0.4 {
                carView.center.x -= 0.9
            } else if horizontalMotion > 0.3 && horizontalMotion < 0.4 {
                carView.center.x += 0.9
            } else if horizontalMotion < -0.35 {
                carView.center.x -= 1.8
            } else if horizontalMotion > 0.35 {
                carView.center.x += 1.8
            }
            if raceStatus.status == false {
                timer.invalidate()
            }
        }
        timer.fire()
    }
    
    func roadMarkingIntersected(myCarImageView: UIImageView,
                                leftRoadSideView: UIView,
                                rightRoadSideView: UIView,
                                raceStatus: RaceStatus,
                                complition: @escaping (Bool) -> ()) {
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            if raceStatus.carStatus {
                if myCarImageView.frame.origin.x < leftRoadSideView.frame.origin.x + leftRoadSideView.frame.size.width {
                    timer.invalidate()
                    complition(true)
                }
                if myCarImageView.frame.origin.x + myCarImageView.frame.size.width > rightRoadSideView.frame.origin.x {
                    timer.invalidate()
                    complition(true)
                }
            }
            if raceStatus.status == false {
                timer.invalidate()
            }
        }
        timer.fire()
    }
    
    func trackBarrierFrame(barrier: UIView,
                           myCarImageView: UIImageView,
                           raceStatus: RaceStatus,
                           complition: @escaping (Bool) -> ()) {
        let timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { (timer) in
            guard let intersection = barrier.layer.presentation()?.frame.intersects(myCarImageView.frame) else {
                return
            }
            if raceStatus.carStatus {
                if intersection {
                    timer.invalidate()
                    complition(true)
                }
            }
            if raceStatus.status == false {
                timer.invalidate()
            }
        }
        timer.fire()
    }
    
    func addCarJump(motionManager: CMMotionManager,
                    carView: UIImageView,
                    raceStatus: RaceStatus,
                    complition: @escaping (Bool) -> ()) {
        motionManager.startGyroUpdates()
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            guard let accelerationX = motionManager.gyroData?.rotationRate.x else {
                return
            }
            if accelerationX > 5 {
                complition(true)
            }
            if raceStatus.status == false {
                timer.invalidate()
            }
        }
        timer.fire()
    }
}

import UIKit

class SaveManager {
    static let shared = SaveManager()
    private init() {}
    
    func saveResults(score: Int,
                     gameDate: Date) {
        if let nickname = UserDefaults.standard.object(forKey: Keys.userNicknames.rawValue) as? String {
            guard let difficultyMultiplier = UserDefaults.standard.object(forKey: Keys.difficulty.rawValue) as? CGFloat else {
                return
            }
            var score = score
            score = Int(Double(score) / Double(difficultyMultiplier) * 100)
            let time = dateToString(gameDate)
            let player = Highscores(name: nickname, score: score, time: time)
            if var playersArray: [Highscores] = UserDefaults.standard.value([Highscores].self, forKey: Keys.classHighScores.rawValue) {
                playersArray.append(player)
                var playersArraySorted = playersArray.sorted {
                    $0.score > $1.score
                }
                if playersArray.count > 10 {
                    playersArraySorted.removeLast()
                    UserDefaults.standard.set(encodable: playersArraySorted, forKey: Keys.classHighScores.rawValue)
                } else {
                    UserDefaults.standard.set(encodable: playersArraySorted, forKey: Keys.classHighScores.rawValue)
                }
            } else {
                var playersArray: [Highscores] = []
                playersArray.append(player)
                UserDefaults.standard.set(encodable: playersArray, forKey: Keys.classHighScores.rawValue)
            }
        }
    }
    
    private func dateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd/MM/yyyy"
        return formatter.string(from: date)
    }
}

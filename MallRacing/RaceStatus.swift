import Foundation

class RaceStatus {
    
    internal init(status: Bool, carStatus: Bool) {
        self.status = status
        self.carStatus = carStatus
    }
    
    var status: Bool
    var carStatus: Bool
}

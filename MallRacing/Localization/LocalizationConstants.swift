import Foundation

let startButtonTitleOne = "Start".locatized()
let startbuttonTitleTwo = " the play".locatized()
let settingsButtonTitle = "Settings".locatized()
let highScoreButtonTitle = "Highscores".locatized()

let alertTitleStringText = "Game over".locatized()
let alertMessageStringText = "Enter your nickname".locatized()
let alertFirstButtonTitle = "Cancel".locatized()
let alertSecondButtonTitle = "Save result".locatized()

let difficultyLevelsLabelText = "Difficulty Levels".locatized()
let easyButtonTitle = "Easy".locatized()
let normalButtonTitle = "Normal".locatized()
let hardButtonTitle = "Hard".locatized()
let insaneButtonTitle = "Insane".locatized()
let barriersLabelText = "Barriers".locatized()

let topResultsLabelText = "Top 10 results".locatized()
let dateLabelText = "Date".locatized()
let nameLabelText = "Name".locatized()
let resultLabelText = "Result".locatized()

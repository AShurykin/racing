import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var highScoresButton: UIButton!
    @IBOutlet weak var majorImageView: UIImageView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillLayoutSubviews() {
        self.checkUserDefaultsStatus()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUIAttributtes()
        addParallaxToView(view: majorImageView, magnitude: 100)
    }
    
    @IBAction private func startButtonPressed(_ sender: UIButton) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController else {
            return
        }
        controller.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction private func settingsButtonPressed(_ sender: UIButton) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController else {
            return
        }
        controller.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction private func highscoresButtonPressed(_ sender: UIButton) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "FourthViewController") as? FourthViewController else {
            return
        }
        controller.modalPresentationStyle = UIModalPresentationStyle.fullScreen
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func setUIAttributtes() {
        let starButtonTitleAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue
            ]
        let startButtonTitleAttributedText = NSMutableAttributedString(string: startButtonTitleOne, attributes: starButtonTitleAttributes)
        let starButtonSecondTitleAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.blue
        ]
        let startButtonTitleSecondAttributedText = NSMutableAttributedString(string: startbuttonTitleTwo, attributes: starButtonSecondTitleAttributes )
        startButtonTitleAttributedText.append(startButtonTitleSecondAttributedText)
        self.startButton.setAttributedTitle(startButtonTitleAttributedText, for: .normal)
        self.settingsButton.setTitle(settingsButtonTitle, for: .normal)
        self.highScoresButton.setTitle(highScoreButtonTitle, for: .normal)
        self.settingsButton.titleLabel?.font = marlboroFont
        self.highScoresButton.titleLabel?.font = marlboroFont
        self.startButton.titleLabel?.font = marlboroFont
        self.view.layoutIfNeeded()
        self.startButton.roundCorner()
        self.startButton.dropShadow()
        self.settingsButton.roundCorner()
        self.settingsButton.dropShadow()
        self.highScoresButton.roundCorner()
        self.highScoresButton.dropShadow()
        self.startButton.addGradient()
        self.settingsButton.addGradient()
        self.highScoresButton.addGradient()
        self.view.layoutIfNeeded()
        self.startButton.titleLabel?.layer.zPosition = 1
        self.settingsButton.titleLabel?.layer.zPosition = 1
        self.highScoresButton.titleLabel?.layer.zPosition = 1
    }
    
    private func addParallaxToView(view: UIImageView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude

        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude

        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
    
    private func checkUserDefaultsStatus() {
        if let _ = UserDefaults.standard.object(forKey: Keys.barrier.rawValue) {
        } else {
            UserDefaults.standard.set(barrierString, forKey: Keys.barrier.rawValue)
        }
        if let _ = UserDefaults.standard.object(forKey: Keys.difficulty.rawValue) {
        } else {
            UserDefaults.standard.set(difficulty, forKey: Keys.difficulty.rawValue)
        }
    }
}


import UIKit

class FourthViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var topResultsLabel: UILabel!
    
    var guardResults: [Highscores] = []
        
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print("\(self) deinitialized")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let results = UserDefaults.standard.value([Highscores].self, forKey: Keys.classHighScores.rawValue) else {
            return
        }
        guardResults = results
        self.setUIAttributes()
    }
    
    @IBAction private func backButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func setUIAttributes() {
        topResultsLabel.text = topResultsLabelText
        dateLabel.text = dateLabelText
        nameLabel.text = nameLabelText
        resultLabel.text = resultLabelText
    }
}

extension FourthViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guardResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HighScoreTableViewCell", for: indexPath) as? HighScoreTableViewCell else {
            return UITableViewCell()
        }
        cell.setPlayerRecord(player: guardResults[indexPath.row])
        return cell
    }
}

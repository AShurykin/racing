import UIKit

class ThirdViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var easyButton: UIButton!
    @IBOutlet weak var normalButton: UIButton!
    @IBOutlet weak var hardButton: UIButton!
    @IBOutlet weak var insaneButton: UIButton!
    @IBOutlet weak var barrierButton: UIButton!
    @IBOutlet weak var ballButton: UIButton!
    @IBOutlet weak var tireButton: UIButton!
    @IBOutlet weak var difficultyLevelsLabel: UILabel!
    @IBOutlet weak var barriersLabel: UILabel!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print("\(self) deinitialized")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.changeAttributes()
        self.setUIAttributes()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func easyButtonPressed(_ sender: UIButton) {
        difficulty = easy
        UserDefaults.standard.set(difficulty, forKey: Keys.difficulty.rawValue)
        changeAttributes()
    }
    
    @IBAction func normalButtonPressed(_ sender: UIButton) {
        difficulty = normal
        UserDefaults.standard.set(difficulty, forKey: Keys.difficulty.rawValue)
        changeAttributes()
    }
    
    @IBAction func hardButtonPressed(_ sender: UIButton) {
        difficulty = hard
        UserDefaults.standard.set(difficulty, forKey: Keys.difficulty.rawValue)
        changeAttributes()
    }
    
    @IBAction func insaneButtonPressed(_ sender: UIButton) {
        difficulty = insane
        UserDefaults.standard.set(difficulty, forKey: Keys.difficulty.rawValue)
        changeAttributes()
    }
    
    @IBAction func barrierButtonPressed(_ sender: UIButton) {
        let barrierImageName = "barrier"
        UserDefaults.standard.set(barrierImageName, forKey: Keys.barrier.rawValue)
        changeAttributes()
    }
    
    @IBAction func ballButtonPressed(_ sender: UIButton) {
        let ballImageName = "football"
        UserDefaults.standard.set(ballImageName, forKey: Keys.barrier.rawValue)
        changeAttributes()
    }
    
    @IBAction func tireButtonPressed(_ sender: UIButton) {
        let tireImageName = "tire"
        UserDefaults.standard.set(tireImageName, forKey: Keys.barrier.rawValue)
        changeAttributes()
    }
    
    private func setUIAttributes() {
        difficultyLevelsLabel.text = difficultyLevelsLabelText
        easyButton.setTitle(easyButtonTitle, for: .normal)
        normalButton.setTitle(normalButtonTitle, for: .normal)
        hardButton.setTitle(hardButtonTitle, for: .normal)
        insaneButton.setTitle(insaneButtonTitle, for: .normal)
        barriersLabel.text = barriersLabelText
    }
    
    private func changeAttributes() {
        ThirdViewControllerManager.shared.changeDifficulty(easyButton, normalButton, hardButton, insaneButton)
        ThirdViewControllerManager.shared.changeBarrier(barrierButton, ballButton, tireButton)
    }
}

import UIKit
import CoreMotion

class SecondViewController: UIViewController {
    
    @IBOutlet weak var leftRoadSideView: UIView!
    @IBOutlet weak var rightRoadSideView: UIView!
    @IBOutlet weak var roadWay: UIView!
    @IBOutlet weak var continuousRoadMarkingView: UIView!
    @IBOutlet weak var continuousRoadMarkingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftContinuousRoadMarkingView: UIView!
    @IBOutlet weak var rightContinuousRoadMarkingView: UIView!
    
    private let myCarImageView = UIImageView()
    private let carConteinerView = UIView()
    private let motionManager = CMMotionManager()
    
    private var objectsForRemoved: [UIView] = []
    private var barriersForRemoved: [UIView] = []
    private var intersectionSelector = false
    private var timer: Timer? = nil
    private var score: Int = 0
    private var gameDate = Date()
    private var duration: CGFloat = 0
    private var carStatus = true
    private var raceStatus = RaceStatus(status: true, carStatus: true)
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print("\(self) deinitialized")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.layoutIfNeeded()
        carConteinerView.isHidden = false
        self.addCarConteinerView()
        self.setAnimationAttributes()
        self.roadMarkingAnimation()
        self.setCarAttributes()
        self.moveCar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SaveManager.shared.saveResults(score: score, gameDate: gameDate)
    }
    
    @objc func backButtonPressed(_ sender: UIButton) {
        raceStatus.status = false
        barriersForRemoved.removeAll()
        timer?.invalidate()
        timer = nil
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func addCarConteinerView() {
        carConteinerView.frame.origin = CGPoint(x: 0, y: 0)
        carConteinerView.frame.size = CGSize(width: view.frame.size.width, height: view.frame.size.height)
        carConteinerView.backgroundColor?.withAlphaComponent(0)
        carConteinerView.layer.zPosition = 1
        let additionalBackButton = UIButton()
        let image = UIImage(named: "close")
        additionalBackButton.frame = CGRect(x: 8, y: 15, width: 30, height: 30)
        additionalBackButton.addTarget(self, action: #selector(backButtonPressed(_:)), for: .touchUpInside)
        additionalBackButton.setImage(image, for: .normal)
        carConteinerView.addSubview(additionalBackButton)
        view.addSubview(carConteinerView)
    }
    
    private func roadMarkingAnimation() {
        guard let roadMarkingAnimationDuration: CGFloat = UserDefaults.standard.object(forKey: Keys.difficulty.rawValue) as? CGFloat else {
            return
        }
        UIView.animate(withDuration: TimeInterval(roadMarkingAnimationDuration)) {
            self.continuousRoadMarkingConstraint.constant = UIScreen.main.bounds.height + 100
            self.view.layoutIfNeeded()
        }
    }
    
    private func setAnimationAttributes() {
        guard let roadMarkingAnimationDuration: CGFloat = UserDefaults.standard.object(forKey: Keys.difficulty.rawValue) as? CGFloat else {
            return
        }
        let continuousRoadMarkingAnimationSpeed = UIScreen.main.bounds.height / roadMarkingAnimationDuration
        let intermittentRoadMarkingsAnimationDuration = (UIScreen.main.bounds.height + continuousRoadMarkingView.frame.size.width * 40) / continuousRoadMarkingAnimationSpeed
        let oneCycleIntermittentRoadMarkingsAnimationDuration = continuousRoadMarkingView.frame.size.width * 40 / continuousRoadMarkingAnimationSpeed
        timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(oneCycleIntermittentRoadMarkingsAnimationDuration), repeats: true) { (_) in
            self.intermittentRoadMarkingsAnimation(oneCycleIntermittentRoadMarkingsAnimationDuration: oneCycleIntermittentRoadMarkingsAnimationDuration, intermittentRoadMarkingsAnimationDuration: intermittentRoadMarkingsAnimationDuration)
            self.addBarriers(intermittentRoadMarkingsAnimationDuration: intermittentRoadMarkingsAnimationDuration)
            self.addRoadSideObjects(intermittentRoadMarkingsAnimationDuration: intermittentRoadMarkingsAnimationDuration)
        }
        timer?.fire()
    }
    
    private func intermittentRoadMarkingsAnimation(
        oneCycleIntermittentRoadMarkingsAnimationDuration: CGFloat,
        intermittentRoadMarkingsAnimationDuration: CGFloat) {
        let intermittentRoadMarkings = UIView(frame: CGRect(
            x: self.continuousRoadMarkingView.frame.origin.x,
            y: 0 - self.continuousRoadMarkingView.frame.size.width * 40,
            width: self.continuousRoadMarkingView.frame.size.width,
            height: self.continuousRoadMarkingView.frame.size.width * 10))
        intermittentRoadMarkings.backgroundColor = .white
        self.setIntermittentRoadMarkingsAttributes(intermittentRoadMarkings)
        duration = intermittentRoadMarkingsAnimationDuration
        UIView.animate(withDuration: TimeInterval(intermittentRoadMarkingsAnimationDuration)) {
            intermittentRoadMarkings.frame.origin.y = UIScreen.main.bounds.height
                + 100
        }
    }
    
    private func setIntermittentRoadMarkingsAttributes(_ intermittentRoadMarkings: UIView) {
        intermittentRoadMarkings.center.x = self.view.center.x
        self.view.addSubview(intermittentRoadMarkings)
        self.objectsForRemoved.append(intermittentRoadMarkings)
        intermittentRoadMarkings.layer.zPosition = self.continuousRoadMarkingView.layer.zPosition
    }
    
    private func addRoadSideObjects(intermittentRoadMarkingsAnimationDuration: CGFloat) {
        let roadSideObject = RoadSideObject()
        let xLeft = leftRoadSideView.frame.origin.x
        let xRight = rightRoadSideView.frame.origin.x
        let xArray = [xLeft, xRight]
        let x = xArray.randomElement()
        guard let xGuard = x else {
            return
        }
        let y = 0 - self.continuousRoadMarkingView.frame.size.width * 40
        let width = leftRoadSideView.frame.size.width * 3 / 5
        let height = width
        roadSideObject.setRoadSideObjectProperties(x: xGuard, y: y, width: width, height: height)
        self.view.addSubview(roadSideObject.roadSideObject)
        UIView.animate(withDuration: TimeInterval(intermittentRoadMarkingsAnimationDuration)) {
            roadSideObject.roadSideObject.frame.origin.y = UIScreen.main.bounds.height
                + 100
        }
    }
    
    private func addBarriers(intermittentRoadMarkingsAnimationDuration: CGFloat) {
        let barrierWidth: CGFloat = CGFloat.random(in: 40 ... 50)
        let barrierHeight: CGFloat = barrierWidth
        let barrier = UIImageView(frame: CGRect(
            x: CGFloat.random(in: self.roadWay.frame.origin.x ... self.roadWay.frame.origin.x + self.roadWay.frame.size.width - barrierWidth),
            y: 0 - self.continuousRoadMarkingView.frame.size.width * 40,
            width: barrierWidth,
            height: barrierHeight))
        guard let imageName = UserDefaults.standard.object(forKey: Keys.barrier.rawValue) as? String else {
            return
        }
        barrier.image = UIImage(named: imageName)
        self.view.addSubview(barrier)
        self.barriersForRemoved.append(barrier)
        score = barriersForRemoved.count
        UIView.animate(withDuration: TimeInterval(intermittentRoadMarkingsAnimationDuration)) {
            barrier.frame.origin.y = UIScreen.main.bounds.height
                + 100
        }
        MotionManager.shared.trackBarrierFrame(barrier: barrier, myCarImageView: myCarImageView, raceStatus: raceStatus) { (count) in
            if count {
                self.timer?.invalidate()
                self.timer = nil
                self.moveToGameOverViewController()
            }
        }
    }
    
    private func setCarAttributes() {
        myCarImageView.image = UIImage(imageLiteralResourceName: "car 2")
        myCarImageView.image = myCarImageView.image?.rotate(radians: .pi/2)
        self.myCarImageView.layoutIfNeeded()
        let myCarImageWidth = roadWay.frame.size.width / 3
        let myCarImageHeight = myCarImageWidth * 2.3
        myCarImageView.frame.size = CGSize(width: myCarImageWidth, height: myCarImageHeight)
        myCarImageView.center = CGPoint(x: view.center.x + roadWay.frame.size.width / 4, y: roadWay.frame.size.height - myCarImageView.center.y)
        self.carConteinerView.addSubview(myCarImageView)
    }
    
    private func moveCar() {
        MotionManager.shared.addHorizontalCarMotions(motionManager: motionManager, carView: myCarImageView, raceStatus: raceStatus)
        MotionManager.shared.roadMarkingIntersected(myCarImageView: myCarImageView, leftRoadSideView: leftRoadSideView, rightRoadSideView: rightRoadSideView, raceStatus: raceStatus) { (count) in
            if count {
                self.timer?.invalidate()
                self.timer = nil
                self.moveToGameOverViewController()
            }
        }
        MotionManager.shared.addCarJump(motionManager: motionManager, carView: myCarImageView, raceStatus: raceStatus) { (count) in
            if count {
                UIView.animate(withDuration: 1, animations: {
                    self.raceStatus.carStatus = false
                    self.myCarImageView.frame.size.width *= 1.5
                    self.myCarImageView.frame.size.height *= 1.5
                }) { (_) in
                    UIView.animate(withDuration: 1, animations: {
                        self.myCarImageView.frame.size.width /= 1.5
                        self.myCarImageView.frame.size.height /= 1.5
                    }) { (_) in
                        self.raceStatus.carStatus = true
                    }
                }
            }
        }
    }
    
    private func moveToGameOverViewController() {
        let pausedTime: CFTimeInterval = view.layer.convertTime(CACurrentMediaTime(), from: nil)
        view.layer.speed = 0.0
        view.layer.timeOffset = pausedTime
        carConteinerView.isHidden = true
        timer?.invalidate()
        let alert = addAlert(titleString: alertTitleStringText, messageString: alertMessageStringText, timer: timer) { (count) in
            if count {
                self.timer?.invalidate()
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        self.present(alert, animated: true)
    }
}

extension SecondViewController {
    func addAlert(titleString: String,
                  messageString: String,
                  firstButtonTitle: String = alertFirstButtonTitle,
                  timer: Timer?,
                  complition: @escaping (Bool) -> ()
    ) -> UIAlertController {
        timer?.invalidate()
        let alertController = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
        let firstButtonAction = UIAlertAction(title: firstButtonTitle, style: .cancel, handler: { action in
            complition(true)
        })
        alertController.addAction(firstButtonAction)
        alertController.addTextField { (textField) in
            textField.text = ""
        }
        let secondButtonAction = UIAlertAction(title: alertSecondButtonTitle, style: UIAlertAction.Style.default) { (_) in
            guard let value = alertController.textFields?.first?.text else {
                return
            }
            UserDefaults.standard.set(value, forKey: Keys.userNicknames.rawValue)
            complition(true)
        }
        alertController.addAction(secondButtonAction)
        return alertController
    }
}

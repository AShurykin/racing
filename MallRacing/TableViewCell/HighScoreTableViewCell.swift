import UIKit

class HighScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var recordLabel: UILabel!
    
    func setPlayerRecord(player: Highscores) {
        dateLabel.text = player.time
        nameLabel.text = player.name
        recordLabel.text = String(player.score)
    }
}

import UIKit

class RoadSideObject {
    
    var roadSideObject = UIImageView()
    
    func setRoadSideObjectProperties(x: CGFloat,
                                     y: CGFloat,
                                     width: CGFloat,
                                     height: CGFloat) {
        roadSideObject.frame = CGRect(x: x, y: y, width: width, height: height)
        let objectsToGetRandomly = [objectEnum.cactus, objectEnum.camel, objectEnum.oilBarrel, objectEnum.palmTree]
        let index = Int(arc4random_uniform(UInt32(objectsToGetRandomly.count)))
        let object = objectsToGetRandomly[index].rawValue
        roadSideObject.image = UIImage(named: object)
    }
    
    enum objectEnum: String {
        case cactus = "cactus"
        case palmTree = "palmTree"
        case oilBarrel = "oilBarrel"
        case camel = "camel"
    }
}
